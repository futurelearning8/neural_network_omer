import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from vis_utils import softmax_crossentropy, softmax_crossentropy_derivative
from sklearn import datasets
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier


class Weights:
    def __init__(self, tensor):
        self.tensor = tensor
        #self.weights = np.random.normal(size=self.tensor)
        self.gradient = np.zeros_like(self.tensor)

class GDOptimizer:
    def __init__(self, batch_size,lr=0.01):
        self.lr = lr
        self.batch_size = batch_size

    def update(self, weights):
        weights.tensor -= self.lr * (weights.gradient )#/ self.batch_size)
        weights.gradient.fill(0)


class Layer:
    def __init__(self):
        self._in = None
        self._out = None
        self._gradient = None
        self._trainable = False

    def forward(self, x):
        self._in = x
        pass
    def backprop(self, dprev):
        pass

class Linear(Layer):
    def __init__(self, input_shape, output_shape):
        super(Linear, self).__init__()
        self.weights = self.init_weights(input_shape, output_shape)

        #self.weights = Weights((input_shape, output_shape))
        self._trainable = True

    def init_weights(self, input_shape, output_shape):
        w = np.random.normal(size=(input_shape, output_shape))
        b = np.zeros(output_shape)
        self.w = Weights(w)
        self.b = Weights(b)

    def forward(self, x):
        self._in=x
        self._out = np.matmul(x,self.w.tensor) +self.b.tensor
        return self._out

    def backprop(self, dprev):
        # first make the calculation of curr grad
        self._gradient = np.matmul(dprev , self.w.tensor.T )
        #backprop the gradient multiply self._in with dprev
        self.w.gradient = np.matmul(self._in.T, dprev)
        self.b.gradient = np.sum(dprev, axis=0, keepdims=True).flatten()
        return self._gradient

class ReLU(Layer):
    def __init__(self):
        super(ReLU, self).__init__()
        self._trainable = False

    def forward(self, x):
        super(ReLU, self).forward(x)
        self._out = np.maximum(0, self._in)
        return self._out

    def backprop(self, dprev):
        return np.maximum(0, self._in > 0) * dprev

class Softmax(Layer):
    def __init__(self):
        super(Softmax, self).__init__()
        self._trainable = False
    def forward(self, x):
        super(Softmax, self).forward(x)
        exp = np.exp(x)
        self._out = exp / exp.sum(axis=1, keepdims=True)
        return self._out
    def backprop(self, dprev):
        return dprev

class NetworkGraph:
    def __init__(self, *layers,batch_size):
        self.layers = layers
        self.optimizer = GDOptimizer(batch_size=batch_size)

    def forward(self, inputs):
        res = inputs
        # calc output of each layer (layer can be activation or linear)
        for layer in self.layers:
            res =layer.forward(x=res)
        return res

    # output = y_pred
    def backprop(self, output ):
        dprev = output.T
        for layer in reversed(self.layers):
            dprev = layer.backprop(dprev)
            # update the weights
            if layer._trainable:
                self.optimizer.update(weights=layer.w)
                self.optimizer.update(weights=layer.b)

    # put bias outside as a layer
    def fit(self,x,y,step_number):
        for i in range(step_number):
            y_pred= self.forward(x)
            if i%1000==0:
                print(y_pred.T)
                print("ping")

            output = y_pred
            #output = np.reshape(y_pred,(-1,2))
            #cost_derivative = output.T - y
            cost = softmax_crossentropy(output,y)
            cost_derivative = softmax_crossentropy_derivative(output,y)
            dprev =  cost_derivative.T

            self.backprop(dprev)

        y_pred=np.argmax(y_pred,axis=1)
        print(np.mean(y_pred.T-y))
        print("pong")

def make_classification_dataset_simple():
    df = pd.read_excel('data/my_simple_dataset.xlsx')
    array = df.to_numpy()
    X = array[:,:-1]
    y = array[:,-1]
    return X,y


if __name__ == '__main__':
    #X, y = make_classification_dataset_simple()
    data = np.array([[2, 3, 5], [4, 4, 8], [4, 2, 6], [2, 7, 9]])
    X = np.array(data[:, :-1])
    y = np.array(data[:, -1])
    X, y = datasets.make_moons(1024, noise=0.1)
    #X, y = make_classification_dataset_simple()
    neurons_num = 24
    batch_size = X.shape[0]
    features_num = X.shape[1]
    #layers = [Linear(features_num, 1)]#, Linear(neurons_num, 1)]
    layers = [Linear(features_num, neurons_num), ReLU(), Linear(neurons_num, 2),Softmax()]

    nn = NetworkGraph(*layers,batch_size=batch_size)
    nn.fit(X,y,step_number=800)

    #TODO
    # add softmax and its cross entropy loss ; check with more complex function- calc acc normaly ; test stuff

    # predict on training set
    #preds = nn.predict(X)

    #print(accuracy_score(y, preds))

    # visualize decision boundary
    #plot_decision_boundary(X, y, lambda x: nn.predict(x))